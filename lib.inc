section .data

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    mov cl, [rdi + rax]
    inc rax
    test cl, cl
    jnz .loop
    dec rax
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rcx, rsp        ;rcx-указатель
    push rax            ;выделение места в стеке
    push rax            ;выделение места в стеке
    push rax            ;выделение места в стеке
    dec rcx             ;0-терминатор

    mov rax, rdi
    mov rdi, 10         ;делитель
.loop:                  ;сохранить все цифры в стек как строку
    xor rdx, rdx
    div rdi
    add dl, '0'         ;перевод в ascii
    dec rcx
    mov [rcx], dl
    test rax, rax
    jnz .loop

    mov rdi, rcx
    call print_string
    pop rax             ;чистка стека
    pop rax             ;чистка стека
    pop rax             ;чистка стека
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .positive
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
.positive:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
.loop:
    mov al, [rdi+r8]
    mov cl, [rsi+r8]
    cmp al, cl
    jne .not_equals
    inc r8
    test al, al
    jnz .loop
    mov rax, 1      ;if equals
    ret
.not_equals:
    xor rax, rax    ;if not equals
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, -1
    jz .error
    pop rax
    ret 
.error:
    xor rax, rax
    add rsp, 8
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
push r12            ;calle-saved regs
push r13
push r14
push r15

mov r15, rdi        ;адрес
mov r12, rdi        ;указатель
mov r14, rsi        ;размер
mov r13, rsi        ;осталось
.loop:              ;пропустить все пробелы в начале
    call read_char
    cmp al, ' '
    je .loop
    cmp al, `\t`
    je .loop
    cmp al, `\n`
    je .loop
    test al, al       ;конец потока
    je .failure
    jmp .checking

.loop2:             ;чтение слова
    call read_char
.checking:
    cmp al, ' '    ;конец потока
    je .success
    cmp al, `\n`     ;конец потока
    je .success 
    cmp al, `\t`     ;конец потока
    je .success         
    cmp al, 0       ;конец потока
    je .success

    cmp r13, 2      ;должно оставаться хотя бы 2 байта (для этого символа и для 0)
    jl .failure
    
    mov byte[r12], al
    inc r12
    dec r13
    jmp .loop2

.failure:
    xor rax, rax
    xor rdx, rdx
    jmp .end
.success:
    mov byte[r12], 0
    sub r14, r13
    mov rdx, r14
    mov rax, r15
.end:
    pop r15         ;calle-saved regs recovery
    pop r14
    pop r13
    pop r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:             ;who wrote this GARBAGE test, that uses print_uint fuction to test this function?
                        ;I spent 2 hours trying to figure out what is wrong with my function
    
    xor rdx, rdx        ;длинна в символах
    xor rax, rax        ;число
    xor rcx, rcx        ;итератор

.loop:
    mov r8b, [rdi + rcx]
    test r8b, r8b
    jz .end

    cmp r8b, '0'
    jl .end
    cmp r8b, '9'
    jg .end

    sub r8b, '0'        ;цифра
    imul rax, 10
    add rax, r8

    inc rcx
    inc rdx
    jmp .loop
.end:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative
    call parse_uint
    ret
.negative:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.loop:
    cmp rax, rdx
    je .too_long
    mov cl, [rdi]
    mov [rsi], cl
    inc rdi
    inc rsi
    inc rax
    test cl, cl
    jne .loop
    ret
.too_long:
    xor rax, rax
    ret
